package org.odk.collect.android.utilities;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;
import org.odk.collect.android.model.Commune;
import org.odk.collect.android.model.District;
import org.odk.collect.android.model.Province;
import org.odk.collect.android.model.Village;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by oi on 9/5/14.
 *
 * @author sovannoty_chea
 */
public class PropertiesUtils {

    public static final String HOUSE_HOLD_PATH = "household.path";
    private static final String PATIENT_PATH = "patient.path";
    private static final String ROOT_PATH = "root.path";

    private static Properties properties = new Properties();
    private static File _rootAppDir;
    private static File _patientAppDir;
    private static File _householdAppDir;
    private static Map<String, Integer> _fee = new HashMap<String, Integer>();
    private static String _patientId = null;
    private static String _wellId;

    private static Context mContext;


    //State of Hidden Question 1 = Show, 0 = hide
    private static TextView tv_QuestionText;
    private static TextView tv_QuestionText1;

    //String Location
    private static String locationGPS;

    //Layout of Radio Taste 1
    private static LinearLayout layoutTaste1;
    private static TextView tv_QuestionTast1;

    private static EditText _villeCode;
    private static Province _selectedProvince;
    private static District _selectedDistrict;
    private static Commune _selectedCommune;
    private static Village _selectedVillage;
    private static Spinner sp_province;
    private static Spinner sp_district;
    private static Spinner sp_commune;
    private static Spinner sp_village;
    private static String _newPatientName = null;

    //Set Other for 7 EditText

    private static EditText _q11Ref;
    private static EditText _q12Ref;
    private static EditText _q13Ref;
    private static EditText _q14Ref;
    private static EditText _q15Ref;
    private static EditText _q16Ref;
    private static EditText _q17Ref;

    private static EditText _primaryOther;
    private static TextView _questionPrimaryOther;

    private static EditText _otherDamaged;
    private static TextView _questionOtherDamaged;

    //Reference Key
    private static int _q1Test = 0;
    private static int _q2Test = 0;
    private static int _q3Test = 0;
    private static int _q4Test = 0;
    private static int _q5Test = 0;
    private static int _q6Test = 0;
    private static int _q7Test = 0;


    //address
    private static String addressProvinceCode;
    private static String addressDistrictCode;
    private static String addressCommuneCode;
    private static String addressVillageCode;


    public static Spinner getSp_province() {
        return sp_province;
    }

    public static void setSp_province(Spinner spProvince) {
        sp_province = spProvince;
    }

    public static Spinner getSp_district() {
        return sp_district;
    }

    public static void setSp_district(Spinner spDistrict) {
        sp_district = spDistrict;
    }

    public static Spinner getSp_commune() {
        return sp_commune;
    }

    public static void setSp_commune(Spinner spCommune) {
        sp_commune = spCommune;
    }

    public static Spinner getSp_village() {
        return sp_village;
    }

    public static void setSp_village(Spinner spVillage) {
        sp_village = spVillage;
    }

    public synchronized static String getNewPatientName() {
        return _newPatientName;
    }

    public synchronized static void setNewPatientName(String _newPatientName) {
        PropertiesUtils._newPatientName = _newPatientName;
    }

    public synchronized static void init(final Context context) {
        mContext = context;
        if (properties.isEmpty()) {
            Resources resources = context.getResources();
            AssetManager assetManager = resources.getAssets();
            InputStream inputStream = null;
            // Read from the /assets directory
            try {
                inputStream = assetManager.open("app.properties");
                properties.load(inputStream);
                System.out.println("The properties are now loaded");
                System.out.println("properties: " + properties);
            } catch (IOException e) {
                System.err.println("Failed to open app property file");
                e.printStackTrace();
            } catch (Exception e) {
                System.err.println("Failed to open app property file");
                e.printStackTrace();
            } finally {
                IOUtils.closeQuietly(inputStream);
            }
            if (!properties.isEmpty()) {
                setValue(context);
            }
        }
    }

    private static void setValue(final Context context) {
        final String patientPath = File.separator + (String) properties.get(PropertiesUtils.PATIENT_PATH);
        final String householdPath = File.separator + (String) properties.get(PropertiesUtils.HOUSE_HOLD_PATH);
        setRootAppDir(context.getExternalFilesDir((String) properties.get(PropertiesUtils.ROOT_PATH)));
        File patientDir = new File(_rootAppDir, patientPath);
        File householdDir = new File(_rootAppDir, householdPath);
        FileUtils.createFolder(patientDir.getAbsolutePath());
        FileUtils.createFolder(householdDir.getAbsolutePath());
        setPatientAppDir(patientDir);
        setHouseholdAppDir(householdDir);

    }

    public static File getRootAppDir() {
        return _rootAppDir;
    }

    private static void setRootAppDir(final File rootAppDir) {
        _rootAppDir = rootAppDir;
    }

    public static File getPatientAppDir() {
        return _patientAppDir;
    }

    private static void setPatientAppDir(final File patientAppDir) {
        _patientAppDir = patientAppDir;
    }

    public static File getHouseholdAppDir() {
        return _householdAppDir;
    }

    private static void setHouseholdAppDir(final File householdAppDir) {
        _householdAppDir = householdAppDir;
    }

    public synchronized static Map getFee() {
        return _fee;
    }

    public synchronized static void setFee(final Map fee) {
        _fee = fee;
    }

    public synchronized static String getPatientId() {
        return _patientId;
    }

    public synchronized static void setPatientId(final String patientId) {
        _patientId = patientId;
    }

    public static Province getSelectedProvince() {
        return _selectedProvince;
    }

    public static void setSelectedProvince(Province _selectedProvince) {
        PropertiesUtils._selectedProvince = _selectedProvince;
    }

    public static District getSelectedDistrict() {
        return _selectedDistrict;
    }

    public static void setSelectedDistrict(District _selectedDistrict) {
        PropertiesUtils._selectedDistrict = _selectedDistrict;
    }

    public static Commune getSelectedCommune() {
        return _selectedCommune;
    }

    public static void setSelectedCommune(Commune _selectedCommune) {
        PropertiesUtils._selectedCommune = _selectedCommune;
    }

    public static Village getSelectedVillage() {
        return _selectedVillage;
    }

    public static void setSelectedVillage(Village _selectedVillage) {
        PropertiesUtils._selectedVillage = _selectedVillage;
    }
    public static EditText get_villeCode() {
        return _villeCode;
    }

    public static void set_villeCode(EditText _villeCode) {
        PropertiesUtils._villeCode = _villeCode;
    }

    public static EditText get_q11Ref() {
        return _q11Ref;
    }

    public static void set_q11Ref(EditText _q11Ref) {
        PropertiesUtils._q11Ref = _q11Ref;
    }

    public synchronized static int get_q1Test() {
        return _q1Test;
    }

    public synchronized static void set_q1Test(int _q1Test) {
        PropertiesUtils._q1Test = _q1Test;
    }

    public static int get_q2Test() {
        return _q2Test;
    }

    public static void set_q2Test(int _q2Test) {
        PropertiesUtils._q2Test = _q2Test;
    }

    public static int get_q3Test() {
        return _q3Test;
    }

    public static void set_q3Test(int _q3Test) {
        PropertiesUtils._q3Test = _q3Test;
    }

    public static int get_q4Test() {
        return _q4Test;
    }

    public static void set_q4Test(int _q4Test) {
        PropertiesUtils._q4Test = _q4Test;
    }

    public static int get_q5Test() {
        return _q5Test;
    }

    public static void set_q5Test(int _q5Test) {
        PropertiesUtils._q5Test = _q5Test;
    }

    public static int get_q6Test() {
        return _q6Test;
    }

    public static void set_q6Test(int _q6Test) {
        PropertiesUtils._q6Test = _q6Test;
    }

    public static int get_q7Test() {
        return _q7Test;
    }

    public static void set_q7Test(int _q7Test) {
        PropertiesUtils._q7Test = _q7Test;
    }

    public static EditText get_q12Ref() {
        return _q12Ref;
    }

    public static void set_q12Ref(EditText _q12Ref) {
        PropertiesUtils._q12Ref = _q12Ref;
    }

    public static EditText get_q13Ref() {
        return _q13Ref;
    }

    public static void set_q13Ref(EditText _q13Ref) {
        PropertiesUtils._q13Ref = _q13Ref;
    }

    public static EditText get_q14Ref() {
        return _q14Ref;
    }

    public static void set_q14Ref(EditText _q14Ref) {
        PropertiesUtils._q14Ref = _q14Ref;
    }

    public static EditText get_q15Ref() {
        return _q15Ref;
    }

    public static void set_q15Ref(EditText _q15Ref) {
        PropertiesUtils._q15Ref = _q15Ref;
    }

    public static EditText get_q16Ref() {
        return _q16Ref;
    }

    public static void set_q16Ref(EditText _q16Ref) {
        PropertiesUtils._q16Ref = _q16Ref;
    }

    public static EditText get_q17Ref() {
        return _q17Ref;
    }

    public static void set_q17Ref(EditText _q17Ref) {
        PropertiesUtils._q17Ref = _q17Ref;
    }

    public static String get_wellId() {
        return _wellId;
    }

    public static void set_wellId(String _wellId) {
        PropertiesUtils._wellId = _wellId;
    }

    public static TextView getTv_QuestionText() {
        return tv_QuestionText;
    }

    public static void setTv_QuestionText(TextView tv_QuestionText) {
        PropertiesUtils.tv_QuestionText = tv_QuestionText;
    }

    public static TextView getTv_QuestionText1() {
        return tv_QuestionText1;
    }

    public static void setTv_QuestionText1(TextView tv_QuestionText1) {
        PropertiesUtils.tv_QuestionText1 = tv_QuestionText1;
    }

    public static LinearLayout getLayoutTaste1() {
        return layoutTaste1;
    }

    public static void setLayoutTaste1(LinearLayout layoutTaste1) {
        PropertiesUtils.layoutTaste1 = layoutTaste1;
    }

    public static TextView getTv_QuestionTast1() {
        return tv_QuestionTast1;
    }

    public static void setTv_QuestionTast1(TextView tv_QuestionTast1) {
        PropertiesUtils.tv_QuestionTast1 = tv_QuestionTast1;
    }

    public synchronized static String getLocationGPS() {
        return locationGPS;
    }

    public synchronized static void setLocationGPS(String locationGPS) {
        PropertiesUtils.locationGPS = locationGPS;
    }

    public static EditText getPrimaryOther() {
        return _primaryOther;
    }

    public static void setPrimaryOther(EditText primaryOther) {
        PropertiesUtils._primaryOther = primaryOther;
    }

    public static TextView getQuestionPrimaryOther() {
        return _questionPrimaryOther;
    }

    public static void setQuestionPrimaryOther(TextView questionPrimaryOther) {
        PropertiesUtils._questionPrimaryOther = questionPrimaryOther;
    }

    public static EditText getOtherDamaged() {
        return _otherDamaged;
    }

    public static void setOtherDamaged(EditText otherDamaged) {
        PropertiesUtils._otherDamaged = otherDamaged;
    }

    public static TextView getQuestionOtherDamaged() {
        return _questionOtherDamaged;
    }

    public static void setQuestionOtherDamaged(TextView questionOtherDamaged) {
        PropertiesUtils._questionOtherDamaged = questionOtherDamaged;
    }

    public static String getAddressProvinceCode() {
        return addressProvinceCode;
    }

    public static void setAddressProvinceCode(String addressProvinceCodeValue) {
        addressProvinceCode = addressProvinceCodeValue;
    }

    public static String getAddressDistrictCode() {
        return addressDistrictCode;
    }

    public static void setAddressDistrictCode(String addressDistrictCodeValue) {
        addressDistrictCode = addressDistrictCodeValue;
    }

    public static String getAddressCommuneCode() {
        return addressCommuneCode;
    }

    public static void setAddressCommuneCode(String addressCommuneCodeValue) {
        addressCommuneCode = addressCommuneCodeValue;
    }

    public static String getAddressVillageCode() {
        return addressVillageCode;
    }

    public static void setAddressVillageCode(String addressVillageCodeValue) {
        addressVillageCode = addressVillageCodeValue;
    }
}
