/*
 * Copyright (C) 2009 University of Washington
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.odk.collect.android.widgets;

import java.util.ArrayList;
import java.util.Vector;

import org.javarosa.core.model.SelectChoice;
import org.javarosa.core.model.data.IAnswerData;
import org.javarosa.core.model.data.SelectOneData;
import org.javarosa.core.model.data.helper.Selection;
import org.javarosa.form.api.FormEntryCaption;
import org.javarosa.form.api.FormEntryPrompt;
import org.javarosa.xpath.expr.XPathFuncExpr;
import org.odk.collect.android.R;
import org.odk.collect.android.activities.FormEntryActivity;
import org.odk.collect.android.application.Collect;
import org.odk.collect.android.external.ExternalDataUtil;
import org.odk.collect.android.external.ExternalSelectChoice;
import org.odk.collect.android.utilities.PropertiesUtils;
import org.odk.collect.android.views.MediaLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

/**
 * SelectOneWidgets handles select-one fields using radio buttons.
 * 
 * @author Carl Hartung (carlhartung@gmail.com)
 * @author Yaw Anokwa (yanokwa@gmail.com)
 */
@SuppressLint("NewApi")
public class SelectOneWidget extends QuestionWidget implements
		OnCheckedChangeListener {

	Vector<SelectChoice> mItems; // may take a while to compute
	ArrayList<RadioButton> buttons;
    String fieldName;

	public SelectOneWidget(Context context, FormEntryPrompt prompt) {
		super(context, prompt);
		
        // SurveyCTO-added support for dynamic select content (from .csv files)
        XPathFuncExpr xPathFuncExpr = ExternalDataUtil.getSearchXPathExpression(prompt.getAppearanceHint());
        if (xPathFuncExpr != null) {
            mItems = ExternalDataUtil.populateExternalChoices(prompt, xPathFuncExpr);
        } else {
            mItems = prompt.getSelectChoices();
        }
		buttons = new ArrayList<RadioButton>();
		
		//setOrientation(LinearLayout.HORIZONTAL);
		//setBackgroundColor(Color.LTGRAY);
	

		// Layout holds the vertical list of buttons
		LinearLayout buttonLayout = new LinearLayout(context);
		buttonLayout.setOrientation(LinearLayout.VERTICAL);
		buttonLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT));
		
		// The buttons take up the right half of the screen
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT,0.25f);

        String fieldElementReference = prompt.getFormElement().getBind().getReference().toString();
        fieldName = getFieldID(fieldElementReference);

		String s = null;
		if (prompt.getAnswerValue() != null) {
			s = ((Selection) prompt.getAnswerValue().getValue()).getValue();
		}

		if (mItems != null) {
			for (int i = 0; i < mItems.size(); i++) {
				RadioButton r = new RadioButton(getContext());
				r.setText(prompt.getSelectChoiceText(mItems.get(i)));
				r.setTextSize(TypedValue.COMPLEX_UNIT_DIP, mAnswerFontsize);
				r.setTag(Integer.valueOf(i));
				r.setId(QuestionWidget.newUniqueId());
				r.setEnabled(!prompt.isReadOnly());
				r.setFocusable(!prompt.isReadOnly());
				r.setPadding(0, 10, 0, 10);
				buttons.add(r);

				if (mItems.get(i).getValue().equals(s)) {
					r.setChecked(true);
				}

				r.setOnCheckedChangeListener(this);

				String audioURI = null;
				audioURI = prompt.getSpecialFormSelectChoiceText(mItems.get(i),
						FormEntryCaption.TEXT_FORM_AUDIO);

                String imageURI;
                if (mItems.get(i) instanceof ExternalSelectChoice) {
                    imageURI = ((ExternalSelectChoice) mItems.get(i)).getImage();
                } else {
                    imageURI = prompt.getSpecialFormSelectChoiceText(mItems.get(i), FormEntryCaption.TEXT_FORM_IMAGE);
                }

				String videoURI = null;
				videoURI = prompt.getSpecialFormSelectChoiceText(mItems.get(i),
						"video");

				String bigImageURI = null;
				bigImageURI = prompt.getSpecialFormSelectChoiceText(
						mItems.get(i), "big-image");

				MediaLayout mediaLayout = new MediaLayout(getContext());
				mediaLayout.setAVT(prompt.getIndex(), "." + Integer.toString(i), r, audioURI, imageURI,
						videoURI, bigImageURI);
				mediaLayout.setLayoutParams(params);
           /*     ImageView divider = new ImageView(getContext());
				divider.setBackgroundResource(android.R.drawable.divider_horizontal_bright);
                mediaLayout.addDivider(divider);*/
/*				if (((i/2) != (Math.floor( mItems.size()/2) )) && i%2==0){
					// Last, add the dividing line (except for the last element)
					ImageView divider = new ImageView(getContext());
					divider.setBackgroundResource(android.R.drawable.divider_horizontal_bright);
					//SHOW_DIVIDER_MIDDLE
					mediaLayout.addDivider(divider);
				}*/
				buttonLayout.addView(mediaLayout);
				//buttonLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE | LinearLayout.SHOW_DIVIDER_BEGINNING | LinearLayout.SHOW_DIVIDER_END);
				//buttonLayout.setDividerDrawable(context.getResources().getDrawable(R.drawable.divider));
			}
		}
        if (fieldName.equalsIgnoreCase("taste1")) {
            PropertiesUtils.setLayoutTaste1(buttonLayout);
            buttonLayout.setVisibility(GONE);
            addView(buttonLayout, params);
        } else {
            addView(buttonLayout, params);
        }
    }

	@Override
	public void clearAnswer() {
		for (RadioButton button : this.buttons) {
			if (button.isChecked()) {
				button.setChecked(false);
				return;
			}
		}
	}

	@Override
	public IAnswerData getAnswer() {
		int i = getCheckedId();
		if (i == -1) {
			return null;
		} else {
			SelectChoice sc = mItems.elementAt(i);
			return new SelectOneData(new Selection(sc));
		}
	}

	@Override
	public void setFocus(Context context) {
		// Hide the soft keyboard if it's showing.
		InputMethodManager inputManager = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(this.getWindowToken(), 0);
	}

	public int getCheckedId() {
		for (int i = 0; i < buttons.size(); ++i) {
			RadioButton button = buttons.get(i);
			if (button.isChecked()) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (!isChecked) {
			// If it got unchecked, we don't care.
			return;
		}

		for (RadioButton button : buttons ) {
			if (button.isChecked() && !(buttonView == button)) {
				button.setChecked(false);
			}
		}
        if (fieldName.equalsIgnoreCase("recording_organization")) {
            if (buttons.get(4).isChecked()) {
                PropertiesUtils.set_q1Test(1);
                PropertiesUtils.get_q11Ref().setVisibility(View.VISIBLE);
                PropertiesUtils.getTv_QuestionText().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.set_q1Test(0);
                PropertiesUtils.get_q11Ref().setVisibility(View.INVISIBLE);
                PropertiesUtils.getTv_QuestionText().setVisibility(INVISIBLE);
            }
        }
        if (fieldName.equalsIgnoreCase("fundedby")) {
            if (buttons.get(4).isChecked()) {
                PropertiesUtils.set_q2Test(1);
                PropertiesUtils.get_q12Ref().setVisibility(View.VISIBLE);
                //Log.d("BUNHANN", PropertiesUtils.getTv_QuestionText().getText().toString());
                PropertiesUtils.getTv_QuestionText1().setVisibility(VISIBLE);

            } else {
                PropertiesUtils.set_q2Test(0);
                PropertiesUtils.get_q12Ref().setVisibility(View.INVISIBLE);
                PropertiesUtils.getTv_QuestionText1().setVisibility(INVISIBLE);
            }
        }
        if (fieldName.equalsIgnoreCase("primarypump")) {
            if (buttons.get(8).isChecked()) {
                PropertiesUtils.set_q3Test(1);
                PropertiesUtils.get_q13Ref().setVisibility(View.VISIBLE);
            } else {
                PropertiesUtils.set_q3Test(0);
                PropertiesUtils.get_q13Ref().setVisibility(View.INVISIBLE);
            }
        }

        if (fieldName.equalsIgnoreCase("diameter_of_well")) {
            if (buttons.get(11).isChecked()) {
                PropertiesUtils.set_q4Test(1);
                PropertiesUtils.get_q14Ref().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.set_q4Test(0);
                PropertiesUtils.get_q14Ref().setVisibility(View.INVISIBLE);
            }
        }

        if (fieldName.equalsIgnoreCase("damaged")) {
            if (buttons.get(4).isChecked()) {
                PropertiesUtils.set_q5Test(1);
                PropertiesUtils.get_q15Ref().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.set_q5Test(0);
                PropertiesUtils.get_q15Ref().setVisibility(View.INVISIBLE);
            }
        }

        if (fieldName.equalsIgnoreCase("otherdamaged")) {
            if (buttons.get(0).isChecked()) {
                PropertiesUtils.set_q6Test(1);
                PropertiesUtils.get_q16Ref().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.set_q6Test(0);
                PropertiesUtils.get_q16Ref().setVisibility(View.INVISIBLE);
            }
        }
        if (fieldName.equalsIgnoreCase("taste1")) {
            if (buttons.get(3).isChecked()) {
                PropertiesUtils.set_q7Test(1);
                PropertiesUtils.get_q17Ref().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.set_q7Test(0);
                PropertiesUtils.get_q17Ref().setVisibility(View.INVISIBLE);
            }
        }
        if (fieldName.equalsIgnoreCase("taste")) {
            if (buttons.get(1).isChecked()) {
                PropertiesUtils.getLayoutTaste1().setVisibility(VISIBLE);
                PropertiesUtils.getTv_QuestionTast1().setVisibility(VISIBLE);
            } else {
                PropertiesUtils.getLayoutTaste1().setVisibility(INVISIBLE);
                PropertiesUtils.getTv_QuestionTast1().setVisibility(INVISIBLE);
            }
        }

      /*  if (!buttons.get(0).isChecked() && fieldName.equalsIgnoreCase("q1test")) {
            PropertiesUtils.set_q1Test(0);
            PropertiesUtils.get_q11Ref().setVisibility(View.INVISIBLE);

        } else if (buttons.get(0).isChecked() && fieldName.equalsIgnoreCase("q1test")) {
            PropertiesUtils.set_q1Test(1);
            PropertiesUtils.get_q11Ref().setVisibility(View.VISIBLE);
        }*/
        Collect.getInstance().getActivityLogger().logInstanceAction(this, "onCheckedChanged",
    			mItems.get((Integer)buttonView.getTag()).getValue(), mPrompt.getIndex());
	}

	@Override
	public void setOnLongClickListener(OnLongClickListener l) {
		for (RadioButton r : buttons) {
			r.setOnLongClickListener(l);
		}
	}

	@Override
	public void cancelLongPress() {
		super.cancelLongPress();
		for (RadioButton button : this.buttons) {
			button.cancelLongPress();
		}
	}

}
